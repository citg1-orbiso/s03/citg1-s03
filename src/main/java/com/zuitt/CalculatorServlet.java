//Joshua G. Orbiso BSIT-3 | G1
package com.zuitt;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Servlet implementation class CalculatorServlet
 */
public class CalculatorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		PrintWriter out = res.getWriter();
		out.println("<h1>You are now using the calculator app</h1>"
				+ " To use the app, input two numbers and an operation.<br><br>\r\n"
				+ " Hit the submit button after filling in the details.<br><br>\r\n"
				+ " You will get the result shown in your browser!");
	}		
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException{
    	int num1 = Integer.parseInt(req.getParameter("num1"));
    	int num2 = Integer.parseInt(req.getParameter("num2"));
    	String oprtn = req.getParameter("oprtn");
    	switch(oprtn) {
    	case "add":
    		int total = num1 + num2;
        	PrintWriter out = res.getWriter();
        	out.println("<p>The two numbers inputted: " + num1 + " , " + num2 
        			  + "<br><br>The opertaion used: " + oprtn
        			  + "<br><br>The result is: " + total + "</p");
        	break;
        	
    	case "subtract":
    		int total1 = num1 - num2;
        	PrintWriter out1 = res.getWriter();
        	out1.println("<p>The two numbers inputted: " + num1 + " , " + num2 
        			  + "<br><br>The opertaion used: " + oprtn
        			  + "<br><br>The result is: " + total1 + "</p>");
        	break;
        	
    	case "multiply":
    		int total2 = num1 * num2;
        	PrintWriter out2 = res.getWriter();
        	out2.println("<p>The two numbers inputted: " + num1 + " , " + num2 
        			  + "<br><br>The opertaion used: " + oprtn
        			  + "<br><br>The result is: " + total2 + "</p>");
        	break;
        	
    	case "divide":
    		int total3 = num1 / num2;
        	PrintWriter out3 = res.getWriter();
        	out3.println("<p>The two numbers inputted: " + num1 + " , " + num2 
        			  + "<br><br>The opertaion used: " + oprtn
        			  + "<br><br>The result is: " + total3 + "</p>");
        	break;   
        	
        default:
        	PrintWriter out4 = res.getWriter();
        	out4.println("<p>Invalid input or operation. Please try again.</p>");  
    	}  	
	}  	
	
/*    	
    	if(oprtn == "add") {
    		int total = num1 + num2;
        	PrintWriter out = res.getWriter();
        	out.println("The two nubmers inputted:" + num1 + " " + num2 
        			  + "The opertaion used:" + oprtn
        			  + "The result is:" + total);
    	}
    	else if(oprtn == "subtract") {
    		int total = num1 - num2;
        	PrintWriter out = res.getWriter();
        	out.println("The two nubmers inputted:" + num1 + " " + num2 
        			  + "The opertaion used:" + oprtn
        			  + "The result is:" + total);
    	}
    	else if(oprtn == "multiply") {
    		int total = num1 * num2;
        	PrintWriter out = res.getWriter();
        	out.println("The two nubmers inputted:" + num1 + " " + num2 
        			  + "The opertaion used:" + oprtn
        			  + "The result is:" + total);
    	}
    	else if(oprtn == "divide") {
    		int total = num1 / num2;
        	PrintWriter out = res.getWriter();
        	out.println("The two nubmers inputted:" + num1 + " " + num2 
        			  + "The opertaion used:" + oprtn
        			  + "The result is:" + total);
    	}
    	else {
        	PrintWriter out = res.getWriter();
        	out.println("<p>Invalid input or operation. Please try again.</p>");
    	}
	}	
*/	
	public void init() throws ServletException {
		System.out.println("******************************************");
		System.out.println(" CalculatorServlet has been initialized. ");
		System.out.println("******************************************");
		}
	
	public void destroy(){
		System.out.println("******************************************");
		System.out.println(" CalculatorServlet has been destroyed. ");
		System.out.println("******************************************");
		}
}
